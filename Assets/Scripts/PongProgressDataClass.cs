﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PongProgressDataClass
{
    public int winCount; // for vs. Time and vs. AI Modes
    public int highScore; // for vs. Wall Mode
    public int totalCoin; // for in-game coins to spend IAP
}

public class PongPurchaseDataClass
{
    public List<BallItem> purchasedBallList;
    //public List<BrickItem> purchasedBrickList;
    public List<Sprite> purchasedPitchList;

    public PongPurchaseDataClass() {}
}
