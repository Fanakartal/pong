﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RootGames.GameStates
{
    public class GameStateScript
    {
        public enum GameState
        {
            InMainMenu,
            InShop,
            InGame,
            Waiting,
            Playing,
            Won,
            Lost,
            InGameMenu,
            Draw
        };

        /*public static GameStateScript(GameState gameState)
        {
            gameState = GameState.InMainMenu;
        }*/
    }
}