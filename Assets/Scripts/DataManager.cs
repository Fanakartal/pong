﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager : MonoBehaviour {

    private PongProgressDataClass pongProgressData;

    private PongPurchaseDataClass pongPurchaseData;

    //private string gameDataFileName = "data.json";

    // Use this for initialization
	void Start () 
    {
        //LoadPlayerProgress();

        LoadBinaryGameData();

        //LoadPlayerPurchaseData();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SubmitNewPlayerProgress(int newHighScore, int newWinCount, int addedCoin)
    {
        if(newHighScore > pongProgressData.highScore)
        {
            pongProgressData.highScore = newHighScore;
            //SavePlayerProgress();
            SaveBinaryGameData();
        }

        if (newWinCount > pongProgressData.winCount)
        {
            pongProgressData.winCount = newWinCount;
            //SavePlayerProgress();
            SaveBinaryGameData();
            Debug.Log("New WinCount saved!.");
        }

        pongProgressData.totalCoin += addedCoin;
        //SavePlayerProgress();
        SaveBinaryGameData();

    }

    // IAPObject: a buyable object from shop (will change to IAP data type)
    public void SpendCoinsOnIAP(int/*IAPObject*/ iAPObject)
    {
        if (pongProgressData.totalCoin >= iAPObject)//.getValue()
            pongProgressData.totalCoin -= iAPObject;//.getValue()
        
        //SavePlayerProgress();
        SaveBinaryGameData();
    }

    //x Dummy for showing coin change (WILL NOT USE)
    public void DummySpendCoins(int coinsToSpend)
    {
        // Animate the change
        /*for (int i = pongProgressData.totalCoin; i == (pongProgressData.totalCoin - coinsToSpend); i--)
        {
            pongProgressData.totalCoin--;
            Invoke("Waiter", 0.0f);
        }*/

        pongProgressData.totalCoin -= coinsToSpend;

        //SavePlayerProgress();
        SaveBinaryGameData();
    }

    //TODO HighScore for Time Mode - TODO: Sadece atılan en fazla golü değil skoru tutmalı, AI mod için de kodlanmalı.
    public int GetHighScore()
    {
        return pongProgressData.highScore;
    }

    // Longest Rally in Wall Mode 
    public int GetWinCount()
    {
        return pongProgressData.winCount;
    }

    // Total In-Game Coins the player has.
    public int GetTotalCoin()
    {
        return pongProgressData.totalCoin;
    }

    // Saving progress data in binary
    public void SaveBinaryGameData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = new FileStream(Application.persistentDataPath + "/gnopprogressdata.sav", FileMode.Create);

        //pongProgressData = new PongProgressDataClass();

        bf.Serialize(fs, pongProgressData);
        fs.Close();
    }

    // Loading progress data from binary file
    private void LoadBinaryGameData()
    {
        if (File.Exists(Application.persistentDataPath + "/gnopprogressdata.sav"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(Application.persistentDataPath + "/gnopprogressdata.sav", FileMode.Open);

            pongProgressData = bf.Deserialize(fs) as PongProgressDataClass;
            fs.Close();
        }
        else
        {
            pongProgressData = new PongProgressDataClass();
            Debug.LogError("Cannot load game data!");
        }
    }

    // DEPRECATED: Use PlayerPrefs only in Player Local Settings
    [System.Obsolete]
    private void LoadPlayerProgress()
    {
        pongProgressData = new PongProgressDataClass();

        if(PlayerPrefs.HasKey("highScore"))
        {
            pongProgressData.highScore = PlayerPrefs.GetInt("highScore");
        }

        if (PlayerPrefs.HasKey("winCount"))
        {
            pongProgressData.winCount = PlayerPrefs.GetInt("winCount");
            //Debug.Log("WinCount loaded!.");
        }

        if (PlayerPrefs.HasKey("totalCoin"))
        {
            pongProgressData.totalCoin += PlayerPrefs.GetInt("totalCoin");
        }
    }

    // DEPRECATED: Use PlayerPrefs only in Player Local Settings
    [System.Obsolete]
    private void SavePlayerProgress()
    {
        PlayerPrefs.SetInt("highScore", pongProgressData.highScore);
        PlayerPrefs.SetInt("winCount", pongProgressData.winCount);
        
        if (pongProgressData.totalCoin >= 0)
            PlayerPrefs.SetInt("totalCoin", pongProgressData.totalCoin);
        else
            PlayerPrefs.SetInt("totalCoin", 0);
    }
}
