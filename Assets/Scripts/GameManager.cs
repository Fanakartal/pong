﻿using RootGames.GameStates;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;
    
    public GameStateScript.GameState gameState;

    public Sprite chosenPlayerSprite;
    public Sprite brickSprite;
    public Sprite pitchSprite;

    public List<Sprite> playerSpriteList;

    //public GameObject gameNameBall;
    // Use this for initialization
	void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    
    void Start () 
    {
        gameState = GameStateScript.GameState.InMainMenu;
        //gameNameBall.GetComponent<Image>().sprite = playerSprite;
	}

    /*void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex == 0)
            gameState = GameStateScript.GameState.InMainMenu;
    }*/
	
	// Update is called once per frame
	void Update () {
		
	}
}
