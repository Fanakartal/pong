﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RootGames.GameStates;

public class BallController : MonoBehaviour {

    // Float
    public float minSpeed;
    public float yVelocity;
    private float ballReleaseCountdown;

    // Integer
    public int gameOverScore;

    // Static variables
    public static int yourScore;
    private static int enemyScore; 
    private static int yourCoins;
    
    // UI Variables
    public GameObject yourNewCoinText;
    public GameObject yourScoreText;
    public GameObject enemyScoreText;
    public GameObject countdownText;
    
    // References to managers
    public GameObject _gameManager;
    private DataManager _pongDataManager;
    
    // For vs-Time Mode
    private float timeModeCounter;
    public GameObject timeCounterText;

    // For vs-Wall Mode
    private bool wallHit;
    
    // For Collectable
    private bool isCollectableByPlayer;
    
    // Use this for initialization
	void Start () 
    {
        InitiliazeBallVariables();

        StartCoroutine(BallStartCountdown(ballReleaseCountdown));
	}

    // Initiliaze starting variables for BallBrick
    private void InitiliazeBallVariables()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager");
        _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Waiting;

        _pongDataManager = _gameManager.GetComponent<DataManager>();// FindObjectOfType<DataManager>();

        yourScore = 0;
        enemyScore = 0;
        yourCoins = 0;
        gameOverScore = 10;
        ballReleaseCountdown = 3.0f;
        timeModeCounter = 90.0f;
        isCollectableByPlayer = false;

        // for wall mode
        wallHit = true;

        this.gameObject.transform.position = new Vector3(0.0f, -1.116f);//Random.Range(-4f, 1.75f));
    }
	
	// Update is called once per frame
	void Update () 
    {        
        if (_gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Waiting)
        {
            //StartCoroutine(BallStartCountdown(ballReleaseCountdown));
        }

        if (_gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Playing)
        {
            if (MainMenuScript.MODE == 1)
            {
                timeModeCounter -= Time.fixedDeltaTime;
                timeCounterText.GetComponent<Text>().text = Mathf.RoundToInt(timeModeCounter).ToString();

                if (timeModeCounter < 0)
                    timeCounterText.GetComponent<Text>().text = "E.T.";
            }
            
            if (Mathf.Abs(this.gameObject.GetComponent<Rigidbody2D>().velocity.x) > 15.0f ||
                    Mathf.Abs(this.gameObject.GetComponent<Rigidbody2D>().velocity.y) > 15.0f)
            {

                Debug.Log("Velocity.x || Velocity.y Overgrow");

                if (this.gameObject.GetComponent<Rigidbody2D>().velocity.x < 0.0f)
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-minSpeed, yVelocity);
                else
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, yVelocity);
            }

            if (Mathf.Approximately(this.gameObject.GetComponent<Rigidbody2D>().velocity.x, 0.0f))
            {
                Debug.Log("Velocity.x Reset()");
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.1f, yVelocity);
            }
        }

        if (_gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Won
            || _gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Lost
            || _gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Draw)
        {

        }
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        // For BrickBall colliding with Enemy
        if (other.gameObject.tag == "Enemy")
        {
            isCollectableByPlayer = false;
            
            ContactPoint2D contact = other.contacts[0];
            Vector2 localContact = other.gameObject.transform.InverseTransformPoint(contact.point);

            if (localContact.y > 0.0f)
            {
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-minSpeed, yVelocity * (10 * localContact.y));
            }
            else if (localContact.y < 0.0f)
            {
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-minSpeed, yVelocity * (10 * localContact.y));
            }
            else
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-minSpeed, 0.0f);
        }

        // For BrickBall colliding with Player
        if (other.gameObject.tag == "Player")
        {
            //for wall mode
            wallHit = true;

            // For collectable
            isCollectableByPlayer = true;

            ContactPoint2D contact = other.contacts[0];
            Vector2 localContact = other.gameObject.transform.InverseTransformPoint(contact.point);

            if (localContact.y > 0.0f)
            {
                //Debug.Log(localContact.y);
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, yVelocity * (10 * localContact.y));
            }
            else if (localContact.y < 0.0f)
            {
                //Debug.Log(localContact.y);
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, yVelocity * (10 * localContact.y));
            }
            else
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, 0.0f);
        }

        // For Wall mode colliding
        if (other.gameObject.tag == "EnemyWall")
        {
            if (wallHit)
            {
                wallHit = false;
                yourScore++;
                yourScoreText.GetComponent<Text>().text = yourScore.ToString();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // "PlusGoal"
        if (other.gameObject.tag == "PlusGoal")
        {
            yourScore++;
            yourScoreText.GetComponent<Text>().text = yourScore.ToString();

            // For collectable
            isCollectableByPlayer = false;

            // for Time Mode
            if (MainMenuScript.MODE == 1 && timeModeCounter >= 0.0f)
            {
                if (_gameManager.GetComponent<GameManager>().gameState != GameStateScript.GameState.Won
                        || _gameManager.GetComponent<GameManager>().gameState != GameStateScript.GameState.Draw)
                {
                    this.gameObject.transform.position = new Vector3(0.0f, Random.Range(-4f, 1.75f));
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-minSpeed, yVelocity);
                }
            }
            else if(MainMenuScript.MODE == 1 && timeModeCounter < 0.0f)
            {
                if (yourScore > enemyScore)
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Won;
                else if (yourScore < enemyScore)
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Lost;
                else
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Draw;

                coinCalculatorTimeMode();
            }
            // for AI Mode
            else
            {
                if (yourScore == gameOverScore)
                {
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Won;

                    coinCalculatorAIMode();
                }
                else
                {
                    if (_gameManager.GetComponent<GameManager>().gameState != GameStateScript.GameState.Won)
                    {
                        this.gameObject.transform.position = new Vector3(0.0f, Random.Range(-4f, 1.75f));
                        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-minSpeed, yVelocity);
                    }
                }
            }
        }

        // "MinusGoal"
        if (other.gameObject.tag == "MinusGoal")
        {
            enemyScore++;
            enemyScoreText.GetComponent<Text>().text = enemyScore.ToString();

            // For collectable
            isCollectableByPlayer = false;

            // for Time Mode
            if (MainMenuScript.MODE == 1 && timeModeCounter >= 0.0f)
            {
                if (_gameManager.GetComponent<GameManager>().gameState != GameStateScript.GameState.Lost
                        || _gameManager.GetComponent<GameManager>().gameState != GameStateScript.GameState.Draw)
                {
                    this.gameObject.transform.position = new Vector3(0.0f, Random.Range(-4f, 1.75f));
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, yVelocity);
                }
            }
            else if(MainMenuScript.MODE == 1 && timeModeCounter < 0.0f)
            {
                if (yourScore > enemyScore)
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Won;
                else if (yourScore < enemyScore)
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Lost;
                else
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Draw;

                coinCalculatorTimeMode();
            }
            // for AI Mode
            else
            {
                if (enemyScore == gameOverScore)
                {
                    _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Lost;
                    
                    coinCalculatorAIMode();
                }
                else
                {
                    if (_gameManager.GetComponent<GameManager>().gameState != GameStateScript.GameState.Lost)
                    {
                        this.gameObject.transform.position = new Vector3(0.0f, Random.Range(-4f, 1.75f));
                        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, yVelocity);
                    }
                }
            }
        }      

        // For Wall Mode - Score counting
        if (other.gameObject.tag == "WallWin")
        {
            _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Lost;

            coinCalculatorWallMode();
        }

        // For Collectable - coin counting and collect animation
        if (other.gameObject.tag == "Coin")
        {
            if (isCollectableByPlayer)
            {
                yourCoins += 1;
                
                other.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                other.gameObject.GetComponent<ParticleSystem>().Play();
                
                Destroy(other.gameObject, 1f);
            }
        }
    }


    //! It will be used to calculate new coins, selection of watch add to earn (x2) and animate the increase -AI Mode-
    private void coinCalculatorAIMode()
    {
        if (_gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Won)
        {
            yourCoins += 10;
            //yourNewCoinText.GetComponent<Text>().text = " +" + yourCoins + " coins";
        }
        else
        {
            yourCoins -= 1;
            //yourNewCoinText.GetComponent<Text>().text = " " + yourCoins + " coins";
        }

        yourNewCoinText.GetComponent<Text>().text = yourCoins > 0 ? " +" + yourCoins + " coins" : " " + yourCoins + " coins";

        _pongDataManager.SubmitNewPlayerProgress(0, 0, yourCoins); // PlayerPrefs / set -> winCount
    }

    //! It will be used to calculate new coins, selection of watch add to earn (x2) and animate the increase -Time Mode-
    private void coinCalculatorTimeMode()
    {
        if (_gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Won)
        {
            yourCoins += 15;
            yourNewCoinText.GetComponent<Text>().text = " +" + yourCoins + " coins";

            _pongDataManager.SubmitNewPlayerProgress(yourScore, 0, yourCoins); // PlayerPrefs / set -> highScore
        }
        else if (_gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Draw)
        {
            yourCoins += 5;
            yourNewCoinText.GetComponent<Text>().text = " +" + yourCoins + " coins";

            _pongDataManager.SubmitNewPlayerProgress(0, 0, yourCoins); // PlayerPrefs / set -> highScore
        }
        else
        {
            yourCoins -= 1;
            yourNewCoinText.GetComponent<Text>().text = " " + yourCoins + " coins";

            _pongDataManager.SubmitNewPlayerProgress(0, 0, yourCoins); // PlayerPrefs / set -> highScore
        }
    }

    //! It will be used to calculate new coins, selection of watch add to earn (x2) and animate the increase -Wall Mode-
    private void coinCalculatorWallMode()
    {
        yourCoins += yourScore * 1;
        yourNewCoinText.GetComponent<Text>().text = "+ " + yourCoins + " coins";

        _pongDataManager.SubmitNewPlayerProgress(0, yourScore, yourCoins); // PlayerPrefs / set -> winCount
    }

    // Play start countdown timer 3.. 2.. 1.. 0.
    private IEnumerator BallStartCountdown(float waitTime)
    {
        for (int i = (int) waitTime; i > 0; i--)
        {
            countdownText.GetComponent<Text>().text = i.ToString();
            yield return new WaitForSeconds(1.0f);
        }
        //yield return new WaitForSeconds(waitTime);
        countdownText.SetActive(false);

        _gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Playing;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, yVelocity);
    }
}



/*foreach (ContactPoint2D ballContact in other.contacts)
{
    Vector2 ballContactPoint = ballContact.point;

    if (ballContactPoint.y > 0.0f)
    {
        Debug.Log(ballContactPoint.y);
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, yVelocity);
    }
    else if (ballContactPoint.y < 0.0f)
    {
        Debug.Log(ballContactPoint.y);
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, -yVelocity);
    }
    else
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(minSpeed, 0.0f);
}*/