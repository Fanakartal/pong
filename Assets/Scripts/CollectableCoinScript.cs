﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootGames.GameStates;

public class CollectableCoinScript : MenuManager 
{
    
    void Start()
    {
        base.InitiliazeGameManager();
        
        Invoke("DestroyMe", 9.0f);
    }

    private void DestroyMe()
    {
        if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Playing)
            Destroy(this.gameObject);
    } 
}