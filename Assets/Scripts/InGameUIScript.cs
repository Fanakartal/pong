﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootGames.GameStates;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameUIScript : MenuManager {

    //public GameObject gameManager;
    //private DataManager pongDataController;
    
    // For AI and Time Modes
    public GameObject gameOverPanel;
    public GameObject victoryText;
    public GameObject defeatText;

    // For Time mode
    public GameObject drawText;

    // For Wall mode
    public GameObject lastScoreText;
    public GameObject highScoreText;

    // For Collectable
    public GameObject collectableCoin;

    // Use this for initialization
	void Start () 
    {
        base.InitiliazeGameManager();
        //gameManager = GameObject.FindGameObjectWithTag("GameManager");
        //pongDataController = FindObjectOfType<DataManager>();
        
        gameOverPanel.SetActive(false);

        if (MainMenuScript.MODE != 2)
            InvokeRepeating("SpawnCoin", Random.Range(3.5f, 4.5f), Random.Range(8.75f, 10.25f));
	}
	
	// Update is called once per frame
	void Update () 
    {
        ExecInGameUIwrtMODE(MainMenuScript.MODE);
	}

    public override void GetNextLevel(int _level)
    {
        if(_level == 0)
        {
            gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.InMainMenu;            
        }

        /*if(_level == 2)
        {}

        /*if(_level == SceneManager.GetActiveScene().buildIndex)
        {
            gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.Waiting;
        }*/
        
        base.GetNextLevel(_level);
    }

    public void PauseGame()
    {
        Time.timeScale = (Time.timeScale == 1.0f) ? 0.0f : 1.0f; 
    }

    // Execute InGameMenu with respect to chosen MODE
    private void ExecInGameUIwrtMODE(int mode)
    {
        // AI mode
        if(mode == 0)
        {
            if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Won)
            {
                gameOverPanel.SetActive(true);
                defeatText.SetActive(false);

                gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.InGameMenu;
            }

            else if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Lost)
            {
                gameOverPanel.SetActive(true);
                victoryText.SetActive(false);

                gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.InGameMenu;
            }
        }
        // Time mode
        else if (mode == 1)
        {
            if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Won)
            {
                gameOverPanel.SetActive(true);
                defeatText.SetActive(false);
                drawText.SetActive(false);

                gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.InGameMenu;
            }

            else if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Lost)
            {
                gameOverPanel.SetActive(true);
                victoryText.SetActive(false);
                drawText.SetActive(false);
                
                gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.InGameMenu;
            }
            
            else if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Draw)
            {
                gameOverPanel.SetActive(true);
                victoryText.SetActive(false);
                defeatText.SetActive(false);
                
                gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.InGameMenu;
            }
        }
        // Wall mode
        else if (mode == 2)
        {
            if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Lost)
            {
                gameOverPanel.SetActive(true);
                lastScoreText.GetComponent<Text>().text = "SCORE: " + BallController.yourScore;
                highScoreText.GetComponent<Text>().text = "BEST SCORE: " + pongDataManager.GetWinCount().ToString();

                gameManager.GetComponent<GameManager>().gameState = GameStateScript.GameState.InGameMenu;
            }
        }
    }

    private void SpawnCoin()
    {
        if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Playing)
            Instantiate(collectableCoin, getNewSpawnPos(), Quaternion.identity);
    }

    private Vector2 getNewSpawnPos()
    {        
        float posX = Random.Range(-4.5f, 4.5f);
        float posY = Random.Range(-4.5f, 2.25f);
        Vector2 pos = new Vector2(posX, posY);

        return pos;
    }
}
