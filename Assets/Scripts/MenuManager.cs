﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public GameObject gameManager;
    public DataManager pongDataManager;
    
    public MenuManager(){}

    public void InitiliazeGameManager()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        pongDataManager = gameManager.GetComponent<DataManager>();
    }

    public virtual void GetNextLevel(int _level)
    {
        SceneManager.LoadScene(_level);
    }
}
