﻿using RootGames.GameStates;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PongController : MonoBehaviour {

    // Float
    public float yBoundry;
    public float negYBoundry;
    private float speed = 16.0f;

    // Bool
    private bool canMove;

    // GameObject
    public GameObject gameManager;
    public GameObject touchArea;

    // Unity variables
    public Vector2 startPos;
    private Camera mainCam;

    // Use this for initialization
	void Start () 
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        this.gameObject.GetComponent<SpriteRenderer>().sprite = gameManager.GetComponent<GameManager>().chosenPlayerSprite;
        
        canMove = false;
        mainCam = Camera.main;
	}
	
	// Update is called once per frame
	void Update ()
    {

#if CROSS_PLATFORM_INPUT

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        if (Input.GetKeyDown(KeyCode.B))
            SceneManager.LoadScene(0);

        if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Playing)
        {
            if (Input.GetAxis("Vertical") != 0)
            {
                //Debug.Log(Input.GetAxis("Vertical"));
                transform.position = new Vector3(transform.position.x, (transform.position.y + Input.GetAxis("Vertical") * speed * Time.deltaTime), transform.position.z);
            }
        }
#endif

#if MOBILE_INPUT

        switch (gameManager.GetComponent<GameManager>().gameState)
        {
            case GameStateScript.GameState.Playing :
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);         
            
                    switch (touch.phase)
                    {
                        // Record initial touch position.
                        case TouchPhase.Began:                   
                            startPos = touch.position;

                            Debug.Log("Began: " + mainCam.ScreenToWorldPoint(startPos));// + "pixel: " + startPos);
                            Debug.Log("TouchArea: " + touchArea.transform.position);
                            //Debug.Log(touchArea.GetComponent<CircleCollider2D>().OverlapPoint(mainCam.ScreenToWorldPoint(startPos)));

                            if (touchArea.GetComponent<CircleCollider2D>().OverlapPoint(mainCam.ScreenToWorldPoint(startPos))) //touchArea.GetComponent<CircleCollider2D>().bounds.Contains(startPos))
                            {
                                Debug.Log("ContainsFinger");
                                canMove = true;
                            }
               
                            break;
                
                        // Determine direction by comparing the current touch position with the initial one.
                        case TouchPhase.Moved:
                        case TouchPhase.Stationary:
                            if (canMove)
                            {

                                transform.position = new Vector3(transform.position.x, mainCam.ScreenToWorldPoint(touch.position).y, transform.position.z);

                                /*direction = touch.position - startPos;
                                Debug.Log("Moved to: " + direction.normalized.y + " in norm pixels, and " + mainCam.ScreenToWorldPoint(direction.normalized).y + " in world space.");*/
                            }

                            break;
                
                        // Report that a direction has been chosen when the finger is lifted.
                        case TouchPhase.Ended:
                            Debug.Log("Ended");
                            canMove = false;
                            break;
                    }
                }
                break;
            }
#endif

        if (transform.position.y > yBoundry)
            transform.position = new Vector3(transform.position.x, yBoundry, transform.position.z);

        if (transform.position.y < negYBoundry)
            transform.position = new Vector3(transform.position.x, negYBoundry, transform.position.z);
    }
}