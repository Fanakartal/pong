﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootGames.GameStates;

public class EnemyController : MonoBehaviour {

    // Float
    public float yBoundry;
    public float negYBoundry;
    public float speed;
    public float lerpSpeed = 1f;
    private float offset = 0.1f;

    // GameObject
    public GameObject gameManager;
    public GameObject _ball;
    
    // Unity variables
    private Rigidbody2D rigidBody;

    // Use this for initialization
	void Start () 
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        this.gameObject.GetComponent<SpriteRenderer>().sprite = gameManager.GetComponent<GameManager>().chosenPlayerSprite;
        
        rigidBody = this.gameObject.GetComponent<Rigidbody2D>();	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (transform.position.y > yBoundry)
            transform.position = new Vector3(transform.position.x, yBoundry, transform.position.z);

        if (transform.position.y < negYBoundry)
            transform.position = new Vector3(transform.position.x, negYBoundry, transform.position.z);	
	}

    void FixedUpdate()
    {
        if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Playing)
        {
            if (_ball.transform.position.x >= -1.0f)
            {
                if (_ball.transform.position.y > this.gameObject.transform.position.y + offset)
                {
                    if (rigidBody.velocity.y < 0)
                        rigidBody.velocity = Vector2.zero;

                    rigidBody.velocity = Vector2.Lerp(rigidBody.velocity, Vector2.up * speed, lerpSpeed * Time.deltaTime);
                }
                else if (_ball.transform.position.y < this.gameObject.transform.position.y - offset)
                {
                    if (rigidBody.velocity.y > 0)
                        rigidBody.velocity = Vector2.zero;

                    rigidBody.velocity = Vector2.Lerp(rigidBody.velocity, Vector2.down * speed, lerpSpeed * Time.deltaTime);
                }
                else
                {
                    rigidBody.velocity = Vector2.Lerp(rigidBody.velocity, Vector2.zero * speed, lerpSpeed * Time.deltaTime);
                }
            }
        }

        if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Won
            || gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.Lost)
        {
            rigidBody.velocity = new Vector2(0.0f, 0.0f);
        }

        if (gameManager.GetComponent<GameManager>().gameState == GameStateScript.GameState.InGameMenu)
        {
            rigidBody.velocity = new Vector2(0.0f, 0.0f);
        }
    }
}
