﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MenuManager {

    public GameObject gameNameBall;

    public GameObject playButton;
    public GameObject modeButton;
    public List<Sprite> modeSprites;
    public static int MODE = 0;

    public Text bestScore;
    public Text mostWins;
    public Text totalCoin;

    //private DataManager pongDataController;
    
    // Use this for initialization
	void Start () 
    {
        base.InitiliazeGameManager();

        //pongDataController = FindObjectOfType<DataManager>();

        modeButton.GetComponent<Image>().sprite = modeSprites[MODE];

        if(SceneManager.GetActiveScene().buildIndex == 0)
            gameNameBall.GetComponent<Image>().sprite = gameManager.GetComponent<GameManager>().chosenPlayerSprite;

        //bestScore.text = "BEST: " + pongDataManager.GetHighScore().ToString();
        mostWins.text = "LONGEST: " + pongDataManager.GetWinCount().ToString();
        totalCoin.text = pongDataManager.GetTotalCoin().ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeMode()
    {
        modeButton.GetComponent<Image>().sprite = modeSprites[(++MODE) % 3];

        if (MODE == 3)
            MODE = 0;
    }

    public void PlayChosenMode()
    {
        GetNextLevel(MODE + 3);
    }


}
