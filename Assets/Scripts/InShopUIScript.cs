﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BallItem
{
    public string ballName;
    public int ballNumber;
    public Sprite ballSprite;
    public int ballPrice;
    public bool isPurchased;
}

public class InShopUIScript : MenuManager {

    public static int chosenSpriteNumber;

    public Text totalCoinText;
    
    public List<Toggle> ballToggleList;

    public List<BallItem> ballItemList;
    

    // Use this for initialization
	void Start () 
    {
        base.InitiliazeGameManager();

        //TODO: chosenSpriteNumber db'den gelmeli.
        //TODO: public void intiliazePurchasedGoods() { ... }
        ballToggleList[chosenSpriteNumber].isOn = true;

        for (int i = 0; i < ballToggleList.Count; i++)
        {
            if (pongDataManager.GetTotalCoin() >= ballItemList[i].ballPrice
                                                        || ballItemList[i].isPurchased)
                ballToggleList[i].interactable = true;
        }

        //totalCoins = pongDataManager.GetTotalCoin();
        totalCoinText.text = pongDataManager.GetTotalCoin().ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeCharacter(int i)
    {
        // For staying chosen as our current ball sprite
        chosenSpriteNumber = i;

        if (ballItemList[i].isPurchased)
        {
            gameManager.GetComponent<GameManager>().chosenPlayerSprite = ballItemList[i].ballSprite;// gameManager.GetComponent<GameManager>().playerSpriteList[i];
        }
        else
        {
            PurchaseItem(ballItemList[i]);
            gameManager.GetComponent<GameManager>().chosenPlayerSprite = ballItemList[i].ballSprite;
        }
    }

    // Purchase for UI script (spending coins on purchasable item and returns TRUE if successful)
    public bool PurchaseItem(BallItem ballItem)
    {
        if (pongDataManager.GetTotalCoin() >= ballItem.ballPrice)
        {
            // Make the purchase
            //pongDataManager.SpendCoinsOnIAP(ballItem.ballNumber);
            pongDataManager.DummySpendCoins(ballItem.ballPrice);
            ballItem.isPurchased = true;
            totalCoinText.text = pongDataManager.GetTotalCoin().ToString();
            
            return true;
        }
        else
        {
            // Not enough coins
            return false;
        }
    }

    //x Deprecated
    /*public void ChangeCharacterBall(Sprite chosenSprite)
    {
        gameManager.GetComponent<GameManager>().chosenPlayerSprite = chosenSprite;
    }*/
}
